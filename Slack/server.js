var express = require('express');
var path = require('path')

var routes = require('./routes/authorize.routes')

var app = express()

app.set('views', path.join(__dirname,'views'));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/',routes);
app.listen(3000, function () {
	  console.log('App listening on port 3000!')
	});
	
module.exports = app;