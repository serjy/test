var express = require('express')
var Authorize = require('../models/authorize.js')

var client_id= '224113160183.225863306580'
var client_secret= 'e0fc8b951f37f0ce0635ba4ed5d962ff'

module.exports.homepage = function(req,res){
	res.render('Slack_sign_in.pug')
}
	
module.exports.sign_in = function(req,res){
	code = req.query.code
	//Call the function in authorize.js to get the token and channel list
	Authorize.getToken(client_id,client_secret,code,function(err,result){
		if (err){res.json({'ok':'false'})}
		else{
			//callback function return channels -- this is an array of json data
			channels = result;
			number = result.length;
			res.render('Slack_channels.pug',{number:number,channels:channels})
		}
	})
}

module.exports.channel = function(req,res){
	id = req.query.id;
	
	//We need to store all the information, such as token, client_id, client_secret in database.
	//For demo, just put token here.
	//For final version, we need to connect to database, to get the token here.
	token = 'xoxp-224113160183-223912266614-225271681728-4249eff72954b46952eb2cfeb80b1fba';
	
	Authorize.getHistory(token,id,function(err,result){
		if (err){res.json({'ok':'false'})}
		else{
			//callback function return history -- this is an array of json data
			history = result;
			number = result.length;
			res.render('Slack_history.pug',{number:number,history:history})
		}		
	})
	
}




