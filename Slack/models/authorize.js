var express = require('express')
var request = require('request')

module.exports.getToken = function (client_id,client_secret,code,callback){
	//Request access_token using client_id, client_secret, code
	token_url = 'https://slack.com/api/oauth.access?client_id=';
	token_url = token_url + client_id + '&client_secret=' + client_secret + '&code=' + code;
	request({
		url:token_url,
		method:"GET",
		json:true,	
	},function(error,response,body){
//		console.log(body);
		if (body['ok'] == true){
			user_id = body['user_id'];
			access_token = body['access_token'];
			team_name = body['team_name'];
			team_id = body['team_id'];
			scope = body['scope'];
			console.log(user_id,team_name,team_id,scope,access_token);
			
			//Request all the channels using token
			channel_url = 'https://slack.com/api/channels.list?token='+access_token;
			request({
				url:channel_url,
				method:"GET",
				json:true,	
			},function(error,response,body){
				channels = body['channels'];
				
				callback(error,channels);
			});
			
		}
		else{callback(true,body)}
	});

}

module.exports.getHistory = function(token,id,callback){
	//Request History of this channel using channel id and token
	url = 'https://slack.com/api/channels.history?token=' + token +'&channel=' + id;
	request({
		url:url,
		method:"GET",
		json:true,	
	},function(error,response,body){
		history = body['messages'];
		has_more = body['has_more'];
		if (has_more == true){console.log('More Messages!')}
		callback(error,history);
	});
}





