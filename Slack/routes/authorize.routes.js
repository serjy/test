//This is the router of backend

var express = require('express')
var controller = require('../controllers/authorize.controller')
var router = express.Router()


router.get('/',controller.homepage)
//If user clicks the 'sign in with Slack' btn, call the function 'sign_in' in authorize.controller
router.get('/auth',controller.sign_in)
//If user clicks the name of channel, call the function 'channel' in authorize.controller
router.get('/channel',controller.channel)



module.exports = router